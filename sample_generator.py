import random
import numpy as np

class SampleGenerator:

    def __init__(self, params, n):
        self.params = params
        self.n = n

    def from_circle(self):
        r = self.params[0]
        x_0 = self.params[1]
        y_0 = self.params[2]
        points = np.zeros((self.n, 2))

        r_tol = random.uniform(0, r/10.)
        r_rnd = np.random.uniform(r-r_tol, r+r_tol, self.n)
        alpha_rnd = np.random.uniform(0, 360, self.n)
        x_rnd = np.cos(alpha_rnd * np.pi / 180) * r_rnd + x_0
        y_rnd = np.sin(alpha_rnd * np.pi / 180) * r_rnd + y_0

        points[:, 0] = x_rnd
        points[:, 1] = y_rnd
        return points

    def from_bbox(self):
        x_min = self.params[0]
        y_min = self.params[1]
        x_max = self.params[2]
        y_max = self.params[3]
        points = np.zeros((self.n, 2))

        x_rnd = np.random.uniform(x_min, x_max, self.n)
        y_rnd = np.random.uniform(y_min, y_max, self.n)

        points[:, 0] = x_rnd
        points[:, 1] = y_rnd
        return points