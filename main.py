#!/usr/env python
# -*- coding: utf-8 -*-
from USER_PATHS import *
from opals import AddInfo, Normals, EchoRatio, Grid, pyDM, DBH, Export
import opals
import matplotlib.pyplot as plt
import ransac2 as ransac
import numpy as np
from circle import Circle
import ogr, gdal
import os, sys

def main(todos = []):
    if "DTM" in todos:
        norm = Normals.Normals()
        er = EchoRatio.EchoRatio()
        gri = Grid.Grid()

        norm.inFile = AOI_ODM
        norm.neighbours = 8
        norm.searchRadius = 2
        norm.selMode = opals.Types.SelectionMode.quadrant
        norm.run(reset=True)
        er.inFile = AOI_ODM
        er.searchRadius = 2
        er.run(reset=True)
        gri.inFile = AOI_ODM
        gri.filter = "Generic[EchoRatio > 80] && Echo[Last]"
        gri.searchRadius = 5
        gri.selMode = opals.Types.SelectionMode.quadrant
        gri.neighbours = 6
        gri.run(reset=True)


    if "normalizedZ" in todos:
        addi = AddInfo.AddInfo()
        addi.inFile = AOI_ODM
        addi.gridFile = AOI_DTM
        addi.attribute = "NormalizedZ=Z-r[0]"
        addi.resampling = opals.Types.ResamplingMethod.bilinear
        addi.run()

    odm_stems = r"stems.odm"
    if "selectPointCloud" in todos:
        exp = Export.Export()
        exp.inFile = AOI_ODM
        exp.outFile = odm_stems
        exp.filter = "Generic[NormalizedZ > 4] && Generic[NormalizedZ < 6]"
        exp.run(reset=True)
        sys.stdout.flush()

    txt_approximations_z = 'approximation_circles_z.txt'
    shp_estimated = r"estim.shp"
    shp_estimated_z = r"estim_z.shp"
    txt_approximations_pre = r"D:\01_Opals\03_Demo\StemMapping\uav-ransac\area2_approximation_circles.txt"
    odm_stems = r"D:\01_Opals\03_Demo\StemMapping\uav-ransac\area2_Normalized_4-6.odm"
    if "detectCircles" in todos:
        odm_stems = r"D:\01_Opals\03_Demo\StemMapping\uav-ransac\area2_Normalized_4-6.odm"
        dm = pyDM.Datamanager.load(odm_stems, True, False)
        circles = []
        window_size = 15
        overlap = 2
        limits = dm.getLimit()
        xmin, ymin, xmax, ymax = limits.xmin, limits.ymin, limits.xmax, limits.ymax
        xcurr, ycurr = xmin, ymin
        areacount = np.ceil((xmax-xmin)/window_size) * np.ceil((ymax-ymin)/window_size)
        i = 0
        print "\n-------------------\nSplitting area into %d tiles." % areacount
        while xcurr < xmax:
            while ycurr < ymax:
                currwindow = "Region[{} {} {} {}]".format(xcurr - overlap/2,
                                                          ycurr - overlap/2,
                                                          xcurr + window_size + overlap/2,
                                                          ycurr + window_size + overlap/2)
                fil = pyDM.Filter(currwindow)
                ptsList = []
                for point in dm.points():
                    if fil.validate(point):
                        ptsList.append([point.x-xmin, point.y-ymin, 0])  # 0=Status: use for ransac


                print "%s points loaded for detection (tile %s) " % (len(ptsList), i)
                if len(ptsList) > 100:
                    ptsList = np.array(ptsList)

                    circles_curr = ransac.main(ptsList, False)
                    circles += [Circle(c.r, c.x_0 + xmin, c.y_0 + ymin) for c in circles_curr]
                else:
                    print "<100 points in tile. Skipping it."
                ycurr += window_size
                i+=1
            ycurr = ymin
            xcurr += window_size

        dm = None

        to_remove_list = []
        for idx in range(len(circles)):
            # suche ueberschneidungen
            for idx2 in range(idx+1, len(circles)):
                if circles[idx].intersects(circles[idx2]):
                    to_rem_idx = idx if circles[idx].r<circles[idx2].r else idx2
                    to_remove_list.append(to_rem_idx)

        circles = [v for i, v in enumerate(circles) if i not in to_remove_list]

        fig = plt.figure(figsize=(10, 10))
        axes = plt.subplot(111)
        for circ in circles:
            axes.add_artist(plt.Circle((circ[1], circ[2]), circ[0],
                                       color='green', fill=True))
        #plt.plot(all_points[:, 0], all_points[:, 1], 'bo', markersize=1)
        #plt.show()

        with open(txt_approximations_pre, 'w') as f:
            for circ in circles:
                x = circ.x_0
                y = circ.y_0
                r = circ.r
                f.write('%.3f %.3f 0. 0. 0. 1. %.3f\n' % (x, y, r))

        driver = ogr.GetDriverByName('ESRI Shapefile')
        if os.path.isfile(SHP_FILENAME):
            driver.DeleteDataSource(SHP_FILENAME)
        ds_out = driver.CreateDataSource(SHP_FILENAME)
        layer_out = ds_out.CreateLayer(os.path.basename(SHP_FILENAME).replace('.shp', ''), geom_type=ogr.wkbPolygon)
        fdef_out = layer_out.GetLayerDefn()
        for c in circles:
            layer_out.CreateFeature(c.toFeature(fdef_out))
        layer_out = None
        ds_out = None

    txt_approximations_z = r"D:\01_Opals\03_Demo\StemMapping\uav-ransac\area2_approximation_circles_z.txt"
    if "adjustFromDTM" in todos:
        with open(txt_approximations_pre, 'r') as fin:
            with open(txt_approximations_z, 'w') as fout:
                for line in fin.readlines():
                    (x,y,z,xa,ya,za,r) = [float(elem) for elem in line.split(" ")]
                    #src_ds = gdal.Open(AOI_DTM)
                    #gt = src_ds.GetGeoTransform()
                    #rb = src_ds.GetRasterBand(1)
                    #px = int((x - gt[0]) / gt[1])  # x pixel
                    #py = int((y - gt[3]) / gt[5])  # y pixel
                    #z = rb.ReadAsArray(px, py, 1, 1) + 1.3 # breast height
                    z = 4
                    fout.write('%.3f %.3f %.3f %.3f %.3f %.3f  %.3f\n' % (x, y, z, x, y, z+6, r))
    shp_estimated = r"D:\01_Opals\03_Demo\StemMapping\uav-ransac\area2_shp_estimations.shp"
    if "getDBH" in todos:
        dbh = DBH.DBH()
        dbh.inFile = odm_stems
        dbh.approxFile = txt_approximations_z
        dbh.outFile = shp_estimated
        dbh.run()
    shp_estimated_z = r"D:\01_Opals\03_Demo\StemMapping\uav-ransac\area2_shp_estimations_z.shp"
    if "convertTo3DSHP" in todos:
        driver = ogr.GetDriverByName("ESRI Shapefile")
        ds_in = driver.Open(shp_estimated, 0)
        lyr_in = ds_in.GetLayer()
        if os.path.exists(shp_estimated_z):
            driver.DeleteDataSource(shp_estimated_z)
        ds_out = driver.CreateDataSource(shp_estimated_z)
        lyr_out = ds_out.CreateLayer(os.path.basename(shp_estimated_z).replace(".shp", ""), geom_type=ogr.wkbLineString25D)

        for feat in lyr_in:
            geom = feat.GetGeometryRef()
            for i in range(100, 201, 10): # every 10 cm
                feat_out = feat.Clone()
                geom_out = ogr.Geometry(ogr.wkbLineString25D)
                for j in range(geom.GetPointCount()):
                    point = geom.GetPoint(j)
                    geom_out.AddPoint(point[0], point[1], i/100.)
                feat_out.SetGeometry(geom_out)
                lyr_out.CreateFeature(feat_out)
                feat_out = None

            for i in range(0,geom.GetPointCount(), 2): # every 2 points
                point = geom.GetPoint(i)
                feat_out = feat.Clone()
                geom_out = ogr.Geometry(ogr.wkbLineString25D)
                geom_out.AddPoint(point[0], point[1], 1.)
                geom_out.AddPoint(point[0], point[1], 2.)
                feat_out.SetGeometry(geom_out)
                lyr_out.CreateFeature(feat_out)
                feat_out = None




if __name__ == "__main__":
    main([
          #"DTM",
          #"normalizedZ",
          #"selectPointCloud",
          #"detectCircles",
          "adjustFromDTM",
          "getDBH",
          "convertTo3DSHP",
          ])