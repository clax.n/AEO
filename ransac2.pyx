from circle import Circle
from sample_generator import SampleGenerator
import numpy as np
import matplotlib.pyplot as plt
import random
import copy

PRINT_DEBUG = False

def _print(str):
    if PRINT_DEBUG:
        print str

# TODO: find intersecting circles - take the largest one
def apply_ransac(points, n, maxRad=0, minPoints=0):
    score = 0
    points[:, 2] = 0
    #tol = 0.1
    tol = 0.1
    params = []
    idx_li = []
    i = 0
    m = points.shape[0]
    points_id_rng = np.array([range(0, m)])
    while i < n:
        idx_ransac_points = np.array([points[:, 2] == 0])
        if len(points_id_rng[idx_ransac_points]) < 4:
            break
        idx_rnd = random.sample(points_id_rng[idx_ransac_points], 3)  # take sample from the lines where status == 0
        points_curr = points[idx_rnd, :]
        try:
            circle_curr = Circle.from_points(points_curr)
        except np.linalg.linalg.LinAlgError:
            continue

        if maxRad > 0 and circle_curr.r > maxRad:  # check radius, if bigger than maxRad, skip this combination
            #i += 1
            continue

        inlier = 0
        idx_li_curr = []
        idx_pts_within = []
        for p_idx in range(0, m):
            is_inlier = circle_curr.is_band_inlier(points[p_idx, :2], tol)
            inlier += int(is_inlier)
            if is_inlier:
                idx_li_curr.append(p_idx)
            is_within = circle_curr.is_point_within(*points[p_idx, :2])
            if is_within:
                idx_pts_within.append(p_idx)

        score_curr = inlier/circle_curr.tol_annulus(tol)
        _print(score_curr)
        if (score_curr > score) and (inlier > minPoints):
            if len(idx_pts_within) > len(idx_li_curr):  # Apply Brandon-Criterea: More points need to be in the band
                # than within the whole circle
                # if this is the case, don't use those points again in this ransac run.
                points[np.unique(idx_li_curr + idx_pts_within), 2] = 1
                if np.sum(points[:, 2]) == points.shape[0]:
                    _print('no more ransac points')
                    break
                _print('continue')
                continue  # try again
            _print("new highscore")
            res_circ = Circle.from_points(points[idx_li_curr, :])
            res_circ_params = res_circ.get()
            if circle_curr.get_distance_from_center(*res_circ_params[1:]) < circle_curr.r:
                _print(" is inlier")
                score = score_curr
                idx_li = idx_li_curr
                params = circle_curr.get()
            else:
                _print(" is not inlier")
        i += 1
    _print(score)
    return params, idx_li, score

def enlarge_circle(circ_points, points, max_dist):
    idx_list_added = []
    while 1:
        circ = Circle.from_points(circ_points)
        d_curr = max_dist + circ.r
        nextp = None
        nextidx = None
        for idx, pt in enumerate(points):
            diff = np.abs(circ_points - np.repeat(np.array([pt]), circ_points.shape[0], axis=0))
            if np.min(diff[:,:2]) > 0:
                d = circ.get_distance_from_center(*pt[:2])
                if d < d_curr:
                    d_curr = d
                    nextp = pt
                    nextidx = idx
        if nextp is not None:
            _print("brandon says: %s" % nextidx)
            circ_points = np.concatenate((circ_points, np.array([nextp.T])), axis=0)
            idx_list_added.append(nextidx)
        else:
            return circ, idx_list_added


def main(points_all = None, showPlots = False):
    if points_all is None:
        # todo: adapt this to use "status" flag
        SG = SampleGenerator([4, 50, 50], 50)
        # circles
        points_C1 = SG.from_circle()
        SG.params = [2, 10, 80]
        points_C2 = SG.from_circle()
        SG.params = [2, 80, 10]
        points_C3 = SG.from_circle()
        SG.params = [8, 25, 10]
        points_C4 = SG.from_circle()
        # area
        SG.params = [0, 0, 100, 100]
        SG.n = 200
        points_C5 = SG.from_bbox()

        points_all = np.concatenate((points_C1, points_C2, points_C3, points_C4, points_C5), axis=0)
        points_all = np.concatenate((points_all, np.zeros((points_all.shape[0], 1))), axis=1)

    # fig = plt.figure(figsize=(10, 10))
    # axes = plt.subplot(111)
    # plt.plot(points_all[:, 0], points_all[:, 1], 'bo', markersize=1)
    # plt.xlabel('x')
    # plt.ylabel('y')
    # plt.show()
    pts_full = copy.copy(points_all)
    cnt = 0
    show_every = 1000000
    circ_list = []
    #try:
    if 1:
        while True:
            if cnt%show_every == 0 and showPlots:
                fig = plt.figure(figsize=(10, 10))
                axes = plt.subplot(111)
            params, idx_li, score = apply_ransac(points_all, 100, 1, 6)
            cnt += 1
            if idx_li:
                res_circ, indices_added = enlarge_circle(points_all[idx_li, :], points_all, 0.2)
                res_circ_params = res_circ.get()

                if (len(idx_li)/res_circ.get_perimeter()) < 5: # TODO
                    break

                circ_list.append(res_circ)
                if showPlots:
                    axes.add_artist(plt.Circle((params[1], params[2]), params[0], color='green', fill=False))
                    axes.add_artist(plt.Circle((res_circ_params[1], res_circ_params[2]), res_circ_params[0],
                                               color='blue', fill=False))
                    plt.annotate(str(cnt), xy=[params[1], params[2]+params[0]])
                    plt.annotate("m: %.5f" % (len(idx_li)/res_circ.get_perimeter()), xy=[params[1], params[2]+params[0] - 0.25])
                    plt.plot(points_all[:, 0], points_all[:, 1], 'bo', markersize=1)
                    plt.plot(points_all[idx_li, 0], points_all[idx_li, 1], 'ro', markersize=1.5)
                    plt.plot(points_all[indices_added, 0], points_all[indices_added, 1], 'go', markersize=5)
                    plt.xlabel('x')
                    plt.ylabel('y')

                    if cnt%show_every == 0:
                        plt.show()

                for idx in indices_added:
                    if idx not in idx_li:
                        idx_li.append(idx)

                for (idx, pt) in enumerate(points_all):
                    if res_circ.is_point_within(*pt) and idx not in idx_li:
                        idx_li.append(idx)
                points_all = np.delete(points_all, idx_li, 0)
            else:
                break

            #except Exception as e:
        _print("The following exception happened:")
    #    print e

    #finally:
        _print("All trees found.")
        if showPlots:
            fig = plt.figure(figsize=(10, 10))
            axes = plt.subplot(111)
        to_remove_list = []
        for idx in range(len(circ_list)):
            # suche ueberschneidungen
            for idx2 in range(idx+1, len(circ_list)):
                if circ_list[idx].intersects(circ_list[idx2]):
                    to_rem_idx = idx if circ_list[idx].r<circ_list[idx2].r else idx2
                    to_remove_list.append(to_rem_idx)

        circ_list = [v for i,v in enumerate(circ_list) if i not in to_remove_list]

        if showPlots:
            for circ in circ_list:
                axes.add_artist(plt.Circle((circ[1], circ[2]), circ[0],
                                           color='green', fill=True))
            plt.plot(pts_full[:, 0], pts_full[:, 1], 'bo', markersize=1)
            plt.show()

        return circ_list

if __name__ == '__main__':
    main()