import numpy as np
import copy
import ogr

class Circle():
    def __init__(self, r, x_0, y_0, avg_height=0):
        self.r = r
        self.r_sq = r**2
        self.x_0 = x_0
        self.y_0 = y_0
        self.avg_height = avg_height

    def __str__(self):
        return 'Circle at (%.1f,%.1f) with radius %.1f' % (self.x_0, self.y_0, self.r)

    def __repr__(self):
        """
        Representation of Circle
        :return: x, y, radius
        """
        return '%.3f %.3f 0. 0. 0. 1. %.3f\n' % (self.x_0, self.y_0, self.r)

    def __getitem__(self, item):
        if item == 0:
            return self.r
        if item == 1:
            return self.x_0
        if item == 2:
            return self.y_0

    def toFeature(self, featuredef):
        feat = ogr.Feature(featuredef)
        geom = ogr.Geometry(ogr.wkbPolygon)
        ring = ogr.Geometry(ogr.wkbLinearRing)
        for angle in range(0, 360):
            angle_rad = angle*np.pi/180.
            x = float(self.x_0 + self.r*np.cos(angle_rad))
            y = float(self.y_0 + self.r*np.sin(angle_rad))
            ring.AddPoint(x, y)
        geom.AddGeometry(ring)
        feat.SetGeometry(geom)
        return feat

    def set(self, r, x_0, y_0):
        self.r = r
        self.x_0 = x_0
        self.y_0 = y_0

    def get(self):
        return [self.r, self.x_0, self.y_0]

    def get_area(self):
        return np.pi*self.r**2

    def get_perimeter(self):
        return 2*np.pi*self.r

    def is_point_within(self, x, y, *args):
        return self.get_distance_from_center(x, y) < self.r

    def get_norm_dist(self, x, y):
        r_1 = self.r
        r_2 = self.get_distance_from_center(x, y)
        return r_2-r_1

    def get_distance_from_center(self, x, y):
        return np.sqrt((x - self.x_0) ** 2 + (y - self.y_0) ** 2)

    def get_norm_dist_sq(self, x, y):
        r_1 = self.r_sq
        r_2 = (x-self.x_0)**2 + (y-self.y_0)**2
        return r_2-r_1

    def is_band_inlier_sq(self, p, tol):
        x, y = p
        norm_dist = self.get_norm_dist_sq(x, y)
        norm_dist_sign = np.sign(norm_dist)
        tol_trans = 2 * self.r * tol + norm_dist_sign * tol ** 2
        return np.abs(norm_dist) < tol_trans

    def is_band_inlier(self, p, tol):
        x, y = p
        norm_dist = np.abs(self.get_norm_dist(x, y))
        return norm_dist < tol

    def tol_annulus(self, tol):
        return np.pi*((self.r + tol) ** 2 - (self.r - tol) ** 2)

    def intersects(self, circle_other):
        [r_other, x0_other, y0_other] = circle_other.get()
        dist_circles = self.get_distance_from_center(x0_other, y0_other)
        r_sum = r_other + self.r
        return dist_circles <= r_sum

    @classmethod
    def from_points(cls, points):
        minx = np.min(points[:, 0])
        miny = np.min(points[:, 1])

        pointscpy = copy.copy(points)

        pointscpy[:, 0] = pointscpy[:, 0] - minx
        pointscpy[:, 1] = pointscpy[:, 1] - miny



        A = np.ones((pointscpy.shape[0], 3))
        l = np.zeros((pointscpy.shape[0], 1))
        A[:, 0] = -2 * pointscpy[:, 0]
        A[:, 1] = -2 * pointscpy[:, 1]
        l[:, 0] = -(pointscpy[:, 0] ** 2 + pointscpy[:, 1] ** 2)
        Qxx = np.dot(A.T, A)
        x = np.dot(np.dot(np.linalg.inv(Qxx), A.T), l)
        a = x[0]
        b = x[1]
        c = x[2]
        r = np.sqrt(a**2 + b**2 - c)
        x_0 = a + minx
        y_0 = b + miny

        measure = 0

        return cls(r, x_0, y_0, measure)

if __name__ == '__main__':
    C = Circle(1, 0, 0)
    print C
    print C.get_area()
    C = Circle.from_points(np.array([[1, 0], [0, 1], [0, -1]]))
    print C
    print C.get_area()
    print C.is_band_inlier_sq((1, 1), 0.45)
