#!/usr/env python
# -*- coding: utf-8 -*-


"""
Setting some paths of files/folders, this does not get synced through GIT and is supposed to stay stable for one machine.
"""
import os

AOI_ODM = r"..\01_data\aoi.odm"  # Path to the input odm
AOI_DTM = r"..\01_data\aoi_dtm.tif"  # Path to the DTM
TEMP_DIR = r"..\03_temp"  # Path to a temporary directory
RESULT_DIR = r"..\04_result"  # Path to the result directory
SHP_FILENAME = os.path.join(RESULT_DIR, "approx.shp") # Path to the shapefile